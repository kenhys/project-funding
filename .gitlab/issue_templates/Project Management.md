Instructions: This template is intended as a guide for creating a project
management issue for an approved project.

Replace any placeholders (text enclosed in parentheses) below as you complete
each section of the issue.  Be sure to remove this header section as well.

---

# Project Management

## Related Issues/Documents

(Provide links to the approved project proposal document, request for bid if
bids were requested, winning bid for proposal, and any other documents or issues
which pertain to the project.)

## Additional Instructions

(Provide details of any instructions which may not be evident in the above
related issues/documents.)

/label ~"Project Management"
